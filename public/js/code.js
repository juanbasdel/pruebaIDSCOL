function getClientes() {
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:3000/findClient";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var json = JSON.parse(xhr.responseText);
      llenarTablaClientes(json, "Lista Clientes");
    }
  };
  xhr.send();
}

function getPedidos(cedula) {
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/findProductsClient";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        llenarTAblaProductos(json);
      }
    };
    var data = JSON.stringify({cedula: cedula});
    xhr.send(data);
  }

function llenarTablaClientes(json ,title){
    document.getElementById("title").innerHTML = title;
    document.getElementById("tableContent").innerHTML = "<tr><th>Cedula</th><th>Nombre</th><th>Año</th><th>Dirección</th></tr>";
    json.forEach(element => {
        document.getElementById("tableContent").innerHTML += "<tr>"+
        "<td>"+element.cedulaCliente+"</td>"+
        "<td>"+element.nombreCliente+"</td>"+
        "<td>"+element.anio+"</td>"+
        "<td>"+element.direccion+"</td>"+
        "<td><a href='#openModal' onClick='getPedidos("+element.cedulaCliente+")'>+VER PEDIDOS</a></td>"+
        "<td><a href='#'>EDITAR</a></td>"+
        "<td><a href='#'>ELIMINAR</a></td>"+
        "</tr>";
    });
}

function llenarTAblaProductos(json){
    
    document.getElementById("tableProduct").innerHTML = "<tr><th>Nombre</th><th>Costo</th></tr>";
    json.forEach(element => {
        var costo = '';
        if(element.costo != undefined) costo = element.costo;
        document.getElementById("tableProduct").innerHTML += "<tr>"+
        "<td>"+element.nombre+"</td>"+
        "<td>"+costo+"</td>"+
        "<td><a href='#'>EDITAR</a></td>"+
        "<td><a href='#'>ELIMINAR</a></td>"+
        "</tr>";
    });
}