// Example model

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const prodSchema = new Schema({
  nombre: String,
  costo: Number
});

const orderSchema = new Schema({
  cedulaCliente: Number,
  nombreCliente: String,
  anio: Number,
  direccion: String,
  productos: [prodSchema]
});


orderSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('Order', orderSchema);

