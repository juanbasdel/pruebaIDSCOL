const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Order = mongoose.model('Order');

module.exports = (app) => {
  app.use('/', router);
};

router.get('/', (req, res, next) => {
  // res.render('index', {
  //   title: 'Almacen Ventas',
  // });
});

// Servicio para agregar ordenes 
router.post('/addOrder', (req, res, next) => {
  const orden = new Order ({
    cedulaCliente : req.body.cedula,
    nombreCliente : req.body.nombre,
    anio: req.body.anio,
    direccion: req.body.direccion,
    productos: req.body.productos
  });
  orden.save().then(() => {
    console.log('Orden guardada exitosamente!!');
  }).catch((err) => {
    console.log('Orden no guardada!!' + err);
  });
  res.json(true);
});

// Servicio para actualizar ordenes 
router.post('/updateOrder', (req, res, next) => {
  Order.update({_id: req.body.id},req.body,(err) => {
    if (err) return next(err);
    res.json(true);
  });
});

// Servicio para eliminar ordenes 
router.post('/deleteOrder', (req, res, next) => {
  Order.remove({_id:req.body.id},(err) =>{
    if (err) return next(err);
    res.json(true);
  });
});

// Servicio para actualizar informacion de un cliente 
router.post('/updateClient', (req, res, next) => {
  Order.update({cedulaCliente: req.body.cedula},req.body,(err) => {
    if (err) return next(err);
    res.json(true);
  });
});

// Servicio para actualizar un producto de una orden 
router.post('/updateProduct', (req, res, next) => {
  Order.update({_id: req.body.id,'productos._id': req.body.idProducto},{'$set':
  {
    'productos.$.nombre': req.body.nombre,
    'productos.$.costo': req.body.costo
  }},(err) => {
    if (err) return next(err);
    res.json(true);
  });
});

// Servicio para eliminar un producto de una orden 
router.post('/deleteProduct', (req, res, next) => {
  Order.update({_id:req.body.id},{'$pull':{productos:{_id:req.body.idProducto}}},(err) =>{
    if (err) return next(err);
    res.json(true);
  });
});

//Servicio para listar los clientes con toda su informacion
router.post('/findClient', (req, res, next) => {
  Order.find((err, order) => {
    if (err) return next(err);
    //Lista de cedulas de clientes auxiliar para realizar el filtro
    var clientAux = [];
    var clients = [];
    /**
     * ciclo para evitar el envio de clientes repetidos
     */ 
    for (let i = 0; i < order.length; i++) {
      const element = order[i];
      var cli;
      if (clientAux.length == 0) {
        clientAux.push(element.cedulaCliente);
        cli = {
          cedulaCliente: element.cedulaCliente,
          nombreCliente: element.nombreCliente,
          anio: element.anio,
          direccion: element.direccion
        };
        clients.push(cli);
      } else {
        if (clientAux.indexOf(element.cedulaCliente) != -1) {
          order.splice(i, 1);
          i--;
        } else {
          clientAux.push(element.cedulaCliente);
          cli = {
            cedulaCliente: element.cedulaCliente,
            nombreCliente: element.nombreCliente,
            anio: element.anio,
            direccion: element.direccion
          };
          clients.push(cli);
        }
      }
    }
    res.json(clients);
  });
});

//servicio para listar los productos de un cliente buscando por cedula  
router.post('/findProductsClient', (req, res, next) => {
  
  Order.find({ cedulaCliente: parseInt(req.body.cedula) }, (err, order) => {
    if (err) return next(err);
    //Lista de productos que se va a devolver 
    var products =[];
    // ciclo para sacar los productos del cliente 
    // de las diferentes ordenes y agruparlas en una sola lista
    for (let i = 0; i < order.length; i++) {
      var element = order[i].productos;
      for (let j = 0; j < element.length; j++) {
        element[j]._id = "123456";
        var produc = element[j];
        produc.idOrden = 1254646;
        products.push(produc);
      }
    }
    res.json(products);
  });
});