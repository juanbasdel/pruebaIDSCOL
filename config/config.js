const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'pruebaidscol'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/pruebaidscol-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'pruebaidscol'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/pruebaidscol-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'pruebaidscol'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/pruebaidscol-production'
  }
};

module.exports = config[env];
